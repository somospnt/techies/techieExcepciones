package com.somospnt.excepciones;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TechieExcepcionesApplication {

	public static void main(String[] args) {
		SpringApplication.run(TechieExcepcionesApplication.class, args);
	}

}
