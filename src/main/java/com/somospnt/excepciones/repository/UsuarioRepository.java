package com.somospnt.excepciones.repository;

import com.somospnt.excepciones.domain.Usuario;
import java.util.Optional;

public interface UsuarioRepository {
    
    Optional<Usuario> findById(long id);
    
}
