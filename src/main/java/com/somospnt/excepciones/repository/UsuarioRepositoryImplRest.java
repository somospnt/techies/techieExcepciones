package com.somospnt.excepciones.repository;

import com.somospnt.excepciones.domain.Usuario;
import java.util.Optional;
import org.springframework.stereotype.Repository;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

@Repository
public class UsuarioRepositoryImplRest implements UsuarioRepository {
    
    private final RestTemplate restTemplate = new RestTemplate();

    @Override
    public Optional<Usuario> findById(long id) {
        Usuario usuario;
        try {
            //cuando id == 1, el mock simula lanza un 404 (HttpClientErrorException extends RuntimeException)
            //cuando id == 2, el mock simula lanza un 500 (HttpServerErrorException extends RuntimeException)
            usuario = restTemplate.getForObject("http://demo0717669.mockable.io/usuarios/" + id, Usuario.class);
        } catch (HttpServerErrorException ex) {
            return Optional.empty();
        }
        return Optional.of(usuario);
    }

}
