package com.somospnt.excepciones.domain;

import lombok.Data;

@Data
public class Usuario {

    private Long id;
    private String nombre;

}
