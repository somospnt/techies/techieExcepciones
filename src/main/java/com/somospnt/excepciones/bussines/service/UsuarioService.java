package com.somospnt.excepciones.bussines.service;

import com.somospnt.excepciones.domain.Usuario;
import com.somospnt.excepciones.repository.UsuarioRepository;
import java.util.NoSuchElementException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario buscar(long id) {
        try {
            return usuarioRepository.findById(id).get();
        } catch (HttpClientErrorException ex) {
            throw new NoSuchElementException();
        }
    }

}
