package com.somospnt.excepciones;

import com.somospnt.excepciones.bussines.service.UsuarioService;
import java.util.NoSuchElementException;
import static org.assertj.core.api.Assertions.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TechieExcepcionesApplicationTests {

    @Autowired
    private UsuarioService usuarioSerice;

    @Test
    public void buscar_usuarioNoExiste_lanzaExcepcion() {
        assertThatExceptionOfType(NoSuchElementException.class)
                .isThrownBy(() -> usuarioSerice.buscar(1L));
    }

    @Test
    public void buscar_errorInternoDelServidor_lanzaExcepcion() {
        assertThatExceptionOfType(RuntimeException.class)
                .isThrownBy(() -> usuarioSerice.buscar(2L));
    }

}
