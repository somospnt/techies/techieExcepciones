# Techie excepciones #
* Checkout del proyecto - https://gitlab.com/somospnt/techies/techieExcepciones.git
* Explicacion de ejercicio de excepciones en sala comun(10 min)

## Explicacion del ejercicio. ##
"Tenemos una aplicacion que cuenta que cuenta con un service y un repository, el servicio es un basico buscar usuario por su id, el cual busca el usuario en un servicio rest. En la carpeta de test tenemos dos test que si los corremos estan correctos."
Pero hay algo que no esta bien.

### Ejercicio 1. (15 min) ###
Vamos a ver el primer test.
Vemos que el primer test es busqueda de usuario que no existe y esto me devuelve una excepcion NoSuchElementeException. Esto funciona pero sin enbargo algo no esta feliz.
Dividirse en equipos de 3 y detectar que parte no esta feliz, porque y como arreglarlo.

### Conclusion (5 min) ###
La excepcion es expuesta hacia afuera y refleja la implementacion del repository el cual lanza una excepcion de http. 
La forma de arreglarlo es cachear la excepcion dentro del repo y devolver un optional vacio.
De esta manera al no encontrar un usuario el repo nos devuelve un optional vacio tal cual como hace hoy jpa.
Nos centramos en la interface y no en la implementacion.


## Ejercicio 2. (15 min) ##
Vamos a ver el segundo test.
En el segundo test vemos que buscamos un usuario pero el servicio nos lanza un error irrecuperable ya que lanzo un 500.
Lo mismo que el primero, el test pasa pero algo no esta feliz.
Dividirse en equipos de 3 y detectar que parte no esta feliz, porque y como arreglarlo.

### Conclusion (5 min) ###
El servicio falla de forma silenciosa ya que al servicio fallar me devuelve un optional vacio, como el mismo caso en que el usuario no existe. 
De esta manera no puedo diferenciar cuando el usuario no existe o cuando fallo el servicio
La forma de arreglarlo es cachear la excepcion asi no exponemos la implementacion del repo y lanzar una excepcion generica ejemplo DataAccessException, que nos indique que el servicio fallo sin importar si fue a una bd o un rest o un archivo.
Luego el que debe decidir si trata la excepcion o no es el servicio. 
Y otra cosa importante, vemos que el test pasa igual porque?
Por que estamos comprobando un runtime exception lo cual en verdad esta lanzando un NoSuchElementeException como en el primer caso pero al acertear de manera generica no nos podemos dar cuenta que la implementacion esta mal.
Solucion hacer una comprobacion mas exacta de el comportamiento esperado.

## ¿Que se llevan hoy? (10 min) ##
Fin 
